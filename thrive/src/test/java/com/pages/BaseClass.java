package com.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.utility.BrowserFactory;
import com.utility.ConfigDataProvider;
import com.utility.ExcelDataProvider;
import com.utility.Helper;

public class BaseClass {
	
	public static WebDriver driver;
	public ExcelDataProvider excel;
	public ConfigDataProvider config;
	public static ExtentReports report;
	public static ExtentTest logger;
	public static JavascriptExecutor j;
	@BeforeSuite
	public void setUpSuite()
	{
		excel=new ExcelDataProvider();
		config=new ConfigDataProvider();
		ExtentSparkReporter extent=new ExtentSparkReporter("./Report/"+Helper.getCurrentDateTime()+".html");
		report=new ExtentReports();
		report.attachReporter(extent);
		driver= BrowserFactory.startApplication(driver,config.getBrowser(), config.getStagingURL());
		j = (JavascriptExecutor) driver;
	}
/*@BeforeClass
	public void setup()
	{
		 System.out.println(driver);
	}*/
	/*@AfterMethod
	public void tearDownMethod(ITestResult result)
	{
		if(result.getStatus()==ITestResult.FAILURE)
		{
			//Helper.CaptureScreenshots(driver);
			logger.fail("Test Case Failed", MediaEntityBuilder.createScreenCaptureFromPath(Helper.CaptureScreenshots(driver)).build());
		}
		report.flush();
	}*/
	//@AfterClass
	//public void tearDown()
//	{
//		BrowserFactory.closeBrowser(driver);
//	}

}
