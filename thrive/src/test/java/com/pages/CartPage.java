package com.pages;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.xmlbeans.impl.repackage.EditBuildScript;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.utility.Helper;

public class CartPage extends BaseClass {
	WebDriver driver;
	
	public CartPage(WebDriver ldriver)
	{
		this.driver=ldriver;
	}
	@FindBy(xpath="//i[@class='fa fa-shopping-cart']") WebElement shoppingCart;
	@FindBy(xpath="//p[@class='card-title h5']") WebElement cartItem;
	@FindBy(xpath="(//div[@class='col-6 col-md-6 col-lg-10 text-md-start text-lg-end'])[1]//following-sibling::div") WebElement subTotal;
	@FindBy(xpath="(//div[@class='col-6 col-md-6 col-lg-10 text-md-start text-lg-end'])[2]//following-sibling::div") WebElement taxAmount;
	@FindBy(xpath="(//div[@class='col-6 col-md-6 col-lg-10 text-md-start text-lg-end'])[3]//following-sibling::div") WebElement grantTotal;
	@FindBy(xpath="//button[text()=' Proceed to Payment ']") WebElement proceedtoPay;
	
	public void viewCart()
	{
		Helper.scrollToElement(driver, shoppingCart);
		WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(300));
		wait.until(ExpectedConditions.visibilityOf(shoppingCart));
		Helper.clickElement(shoppingCart);
		if(cartItem.isDisplayed())
		{
			System.out.println("Added item is present in cart");
		}
		else
		{
			System.out.println("Added item is not present in cart");
		}
			
	}
	public void verifyGrantTotal()
	{
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		double SubTotalAmout=Double.parseDouble(subTotal.getText().substring(1));
		double tax=Double.parseDouble(taxAmount.getText().substring(1));
		double grantTotalAmount=Double.parseDouble(grantTotal.getText().substring(1));
		double finalTotalAmount=SubTotalAmout+tax;
		if(finalTotalAmount==grantTotalAmount)
		{
			System.out.println(" grand total is calculated and displayed properly");
		}
		else
		{
			System.out.println(" grand total is not calculated and displayed properly");
		}
	}
	public void orderDetails()
	{
		proceedtoPay.click();
		String alertText=driver.switchTo().alert().getText();
		if(alertText.contains("Your order has been successfully placed"))
		{
			System.out.println("JSON popup is opened successfully");
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.switchTo().alert().accept();
	}
}
