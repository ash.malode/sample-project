package com.pages;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.utility.Helper;

public class HomePage {
	WebDriver driver;
	public HomePage(WebDriver ldriver)
	{
		this.driver=ldriver;
	}
	@FindBy(xpath="//input[@id='searchBox']") WebElement searchBox;
	@FindBy(id="flexSwitchCheckChecked") WebElement vegToggleButton;
	@FindBy(xpath="//i[@ng-reflect-klass='fa fa-square']") WebElement vegItemIcon;
	@FindBy(xpath="//i[@class='fa fa-square text-danger']") List<WebElement> novegItemIcon;
	@FindBy(xpath="//p[@class='card-title h5']") WebElement searchResult;
	@FindBy(xpath="//button[@class='btn btn-primary btn-md']") WebElement addToCart;
	
	public void FilteredbyVeg()
	{
		Helper.scrollToElement(driver, vegToggleButton);
		WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(300));
		wait.until(ExpectedConditions.visibilityOf(vegToggleButton));
		Helper.clickElement(vegToggleButton);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(vegItemIcon.getAttribute("class").contains("fa fa-square text-success"))
		{
			System.out.println("All veg items are sorted correctly");
		}
		else
		{
			System.out.println("veg items are sorted correctly");
		}
	}
	
	public void searchItemByName()
	{
		Helper.scrollToElement(driver, searchBox);
		searchBox.sendKeys("Shami Kebab");
		if(searchResult.isDisplayed())
		{
			System.out.println("Search result is correct");
		}
		else
			{
				System.out.println("Search result is not correct");
			}	
		
	}
	public void addItemToCart()
	{
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Helper.clickElement(addToCart);
	
	}
	
}
