package com.utility;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.FileHandler;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.pages.BaseClass;

public class Helper extends BaseClass{
	

	public static String CaptureScreenshots(WebDriver driver)
	{ 
		String Screenshotpath="./Screenshot/"+getCurrentDateTime()+".png";
		File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		System.out.println("scrennshot");
		try {
			org.openqa.selenium.io.FileHandler.copy(src, new File(Screenshotpath));
		} catch (IOException e) {
			System.out.println("Unable to capture screenshot"+e.getMessage());
		}
		return Screenshotpath;
		
	}
	public static String getCurrentDateTime()
	{
		DateFormat currentFormat=new SimpleDateFormat("MM_dd_yyyy_HH_mm_ss");
		Date currentDate=new Date();
		return currentFormat.format(currentDate);
	}
   public static void scrollToElement(WebDriver driver,WebElement element)
   {System.out.println(j);
	     j.executeScript("arguments[0].scrollIntoView();", element);
   }
   public static void clickElement(WebElement element)
   {
	   j.executeScript("arguments[0].click();", element);
   }
   public static void selectValueFromDrp(List<WebElement> element, String valueToselect)
   {
	   for(WebElement e:element)
		{
			//System.out.println(e.getText());
			if(e.getText().equals(valueToselect))
			{
				e.click();
				break;
			}
		} 
   }
   
 
}
