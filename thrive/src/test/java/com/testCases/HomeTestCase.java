package com.testCases;


import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.pages.BaseClass;
import com.pages.HomePage;
import com.utility.BrowserFactory;


public class HomeTestCase extends BaseClass {

@Test
	public void HomeScreen()
	{
	
		
		 HomePage hp=PageFactory.initElements(driver, HomePage.class);
		/*Items are getting filtered by veg.*/
		 hp.FilteredbyVeg();
		/*Users are able to search for items by name or by category.*/
		 hp.searchItemByName();
		 /*Items are added/removed to/from cart.*/
		 hp.addItemToCart();
	
	}
  
   
}
