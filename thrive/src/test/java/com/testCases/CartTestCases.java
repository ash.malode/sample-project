package com.testCases;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.pages.BaseClass;
import com.pages.HomePage;
import com.pages.CartPage;

public class CartTestCases extends BaseClass {
	@Test
	public void selectProduct() throws InterruptedException
	{
		
		 CartPage cp=PageFactory.initElements(driver, CartPage.class);
		/*1. Users are able to view a list of all items added to the cart.*/
		 cp.viewCart();
		/*2. Item subtotal, taxes applied & grand total are calculated and displayed properly.*/
		 cp.verifyGrantTotal();
		/*3. The checkout button upon clicking should open a pop-up containing JSON of final order details.*/
		 cp.orderDetails();
	}
  
}
